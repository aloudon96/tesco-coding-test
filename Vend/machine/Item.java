package machine;

public abstract class Item {

    public abstract String getObjectItemName();
    public abstract double getPrice();
    public abstract String getItemSelector();
    public abstract int getQuantity();
    public abstract void removeOne();


}
