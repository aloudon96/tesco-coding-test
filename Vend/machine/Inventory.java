package machine;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Inventory {

    private int totalItems = 0;
    private List<Item> itemList;


    public Inventory(ArrayList<Item> items) {
        this.itemList = items;
        populateTotal();
    }

    public Item selectItem(String selector) {

        Item selected = pickItem(selector);

        if(selected != null) {

            removeItem(selected);
        }

        return selected;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    private void populateTotal () {
        for (Item item : itemList) {
            totalItems += item.getQuantity();
        }
    }

    private void removeItem(Item selectedItem) {

        selectedItem.removeOne();

        if (selectedItem.getQuantity() == 0) {
            itemList.remove(selectedItem);
        }

    }

    private Item pickItem (String selector) {

        Optional<Item> toRemove = itemList.stream().filter(i -> i.getItemSelector() == selector).findFirst();

        return toRemove.orElse(null);

    }

    public String toString() {

        String itemACount = "The number of " + ItemA.getItemNameInMemory() + "s at position A is: " + ItemA.getFinalQuantity();
        String itemBCount = "The number of " + ItemB.getItemNameInMemory() + "s at position B is: " + ItemB.getFinalQuantity();
        String itemCCount = "The number of " + ItemC.getItemNameInMemory() + "s at position C is: " + ItemC.getFinalQuantity();

        return itemACount + "\n" + itemBCount + "\n" + itemCCount;

    }
}
