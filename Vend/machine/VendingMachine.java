package machine;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */
public class VendingMachine {

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    private Double totalInserted = 0.00;
    private Inventory inventory;
    private String status = "OFF";

    private int pounds;
    private int fifties;
    private int twenties;
    private int tens;

    public VendingMachine(Inventory inventory) {
        this.inventory = inventory;
    }

    public void insertCoins(int pounds, int fifties, int twenties, int tens) {

        this.pounds+=pounds;
        this.fifties+=fifties;
        this.twenties+=twenties;
        this.tens+=tens;

        totalInserted += (0.5 * fifties) + (0.2 * twenties) + (0.1 * tens) + (pounds);

    }

    public String listInventory() {
        return this.inventory.toString();
    }

    public String listChange() {
        return "Pounds: " + pounds + "\n" + "Fifties: " + fifties + "\n" + "Twenties: " + twenties + "\n" + "Tens: " + tens;
    }

    public Item dispense(String selector) throws Exception {
        if (isOn()) {
            Item selected = this.inventory.selectItem(selector);
            if (selected != null && selected.getPrice() <= totalInserted) {
                returnChange(selected);
                return selected;
            } else if (selected == null) {
                throw new OutOfStockException("Unfortunately, there are no more of these left. Please select another item");
            } else {
                throw new InsufficientFundsException("Sorry, you do not have enough funds for this item!");
            }
        } else {
            throw new InvalidStateException("The vending machine cannot operate in its current state (" + this.status + ")");
        }
    }

    public int getTotalStock () {
        return this.inventory.getTotalItems();
    }

    public List<Item> getItemsAsList() {
        return this.inventory.getItemList();
    }

    public boolean isOn() {
        return this.status == "ON";
    }

    public void setOn() {
        this.status = "ON";
    }

    public void setOff() {
        this.status = "OFF";
    }

    public double getTotalInserted() {
        return totalInserted;
    }

    private double returnChange (Item item) {

        double remainingChange = 0.00;

        System.out.println("Total money inserted: £" + String.format("%.2f", this.totalInserted));
        System.out.println("Selected item: " + item.getObjectItemName() + " [£" + String.format("%.2f", item.getPrice()) + "]");

        if (item.getPrice() == this.totalInserted) {
            System.out.println("Cost was equal to amount inserted - no change due\n");
        } else {
            remainingChange = (((int) (this.totalInserted * 100) - (item.getPrice() * 100)));
            System.out.println("Change to be returned:  " + "£" + String.format("%.2f", (remainingChange / 100)) + "\n");
        }
        totalInserted -= item.getPrice();
        return remainingChange;
    }
}
