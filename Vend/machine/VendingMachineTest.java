package machine;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {

    Item itemA = new ItemA(3);
    Item itemB = new ItemB(2);
    Item itemC = new ItemC(4);

    ArrayList<Item> itemList = new ArrayList<>(Arrays.asList(itemA, itemB, itemC));

    private Inventory filledInventory = new Inventory(itemList);

    @Test
	public void testInventoryItemCountAsExpected() {
    	VendingMachine machine = new VendingMachine(filledInventory);
    	int actual = machine.getTotalStock();

    	assertEquals(9, actual);
	}

    @Test(expected = InvalidStateException.class)
    public void testDoesNotDispenseIfTurnedOff() throws Exception {
        VendingMachine machine = new VendingMachine(filledInventory);
		assertFalse(machine.isOn());
        machine.dispense("A");
    }

    @Test(expected = InsufficientFundsException.class)
    public void testThrowsInsufficientFundsException() throws Exception {
        VendingMachine machine = new VendingMachine(filledInventory);
        machine.setOn();
        machine.insertCoins(0, 1, 0, 0);
        machine.dispense("A");

        assertTrue(machine.isOn());
    }

	@Test(expected = OutOfStockException.class)
	public void testDispensesItemUntilNoneLeft() throws Exception {
		VendingMachine machine = new VendingMachine(filledInventory);
		machine.setOn();
		machine.insertCoins(3, 0, 0, 0);
		machine.dispense("B");
		machine.dispense("B");

		assertEquals(1, machine.getTotalInserted(), 0);
		assertEquals(2, machine.getItemsAsList().size());
		assertTrue(machine.isOn());

		machine.dispense("B");
	}

    @Test
    public void testDispensesItemWhenTurnedOnAndSufficientFunds() throws Exception {
        VendingMachine machine = new VendingMachine(filledInventory);
        machine.setOn();
        machine.insertCoins(1, 1, 1, 0);
        Item actual1 = machine.dispense("A");

        assertEquals(itemA, actual1);

        assertTrue(machine.isOn());
    }

    @Test
	public void testExactPriceInsertedNoChangeDue() throws Exception {
    	VendingMachine machine = new VendingMachine(filledInventory);
    	machine.setOn();
    	machine.insertCoins(0,1,0,1);
		machine.dispense("A");

		assertEquals(0, machine.getTotalInserted(), 0);
	}

	@Test
	public void testChangeDueWhenFundsRemain() throws Exception {
		VendingMachine machine = new VendingMachine(filledInventory);
		machine.setOn();
		machine.insertCoins(1,0,0,0);
		machine.dispense("A");

		assertTrue(machine.getTotalInserted() > 0);
	}

	@Test
	public void testToStringPrintsCorrectValues() throws Exception {

		VendingMachine machine = new VendingMachine(filledInventory);
		machine.setOn();

		String expected = "The number of Mars Bars at position A is: 3\n" +
						  "The number of Galaxy Caramels at position B is: 2\n" +
						  "The number of Red Bulls at position C is: 4";

		String actual = machine.listInventory();

		assertEquals(expected, actual);

	}

    @Test
    public void testToStringPrintsCorrectValuesAfterDispensing() throws Exception {

		VendingMachine machine = new VendingMachine(filledInventory);
		machine.setOn();

		machine.insertCoins(5, 0, 0, 0);

		String expected = "The number of Mars Bars at position A is: 3\n" +
						  "The number of Galaxy Caramels at position B is: 2\n" +
						  "The number of Red Bulls at position C is: 2";

		for (int i = 2; i > 0; i--) {
			machine.dispense("C");
		}

		String actual = machine.listInventory();

		assertEquals(expected, actual);
    }

    @Test
	public void testDisplaysCorrectChangeAfterCoinsInserted() {

    	VendingMachine machine = new VendingMachine(filledInventory);
    	machine.insertCoins(1,1,1,0);

    	String expected = "Pounds: 1\n" +
						  "Fifties: 1\n" +
						  "Twenties: 1\n" +
						  "Tens: 0";

    	String actual = machine.listChange();

    	assertEquals(expected, actual );

	}

    @Test
    public void defaultStateIsOff() {
        VendingMachine machine = new VendingMachine(filledInventory);
        assertFalse(machine.isOn());
    }

    @Test
    public void turnsOn() {
        VendingMachine machine = new VendingMachine(filledInventory);
        machine.setOn();
        assertTrue(machine.isOn());
    }

	@Test
	public void turnsOff() {
		VendingMachine machine = new VendingMachine(filledInventory);
		machine.setOff();
		assertFalse(machine.isOn());
	}
}
