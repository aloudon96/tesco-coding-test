package machine;

public class ItemA extends Item {

    private String itemSelector = "A";
    private double itemPrice = 0.60;
    private int quantity;

    private static int finalQuantity;
    private String objectItemName = "Mars Bar";

    private static String itemNameInMemory;

    public ItemA (int quantity) {
        this.quantity = quantity;
        ItemA.itemNameInMemory = objectItemName;
        ItemA.finalQuantity = quantity;
    }

    @Override
    public double getPrice() { return this.itemPrice; }

    public String getObjectItemName() {
        return this.objectItemName;
    }

    public int getQuantity() { return quantity; }

    public String getItemSelector() { return this.itemSelector; }

    public void removeOne() {
        this.quantity--;
        ItemA.finalQuantity = this.quantity;
    }

    public static String getItemNameInMemory() { return itemNameInMemory; }

    public static int getFinalQuantity() { return finalQuantity; }

}
