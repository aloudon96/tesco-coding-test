package machine;

public class ItemC extends Item {

    private String itemSelector = "C";
    private double itemPrice = 1.70;
    private int quantity;

    private static int finalQuantity;
    private String objectItemName = "Red Bull";

    private static String itemNameInMemory;

    public ItemC (int quantity) {
        this.quantity = quantity;
        ItemC.itemNameInMemory = objectItemName;
        ItemC.finalQuantity = quantity;
    }

    @Override
    public double getPrice() {
        return this.itemPrice;
    }

    public String getObjectItemName() { return this.objectItemName; }

    public int getQuantity() {
        return quantity;
    }

    public String getItemSelector() {
        return this.itemSelector;
    }

    public void removeOne() {
        this.quantity--;
        ItemC.finalQuantity = this.quantity;
    }

    public static String getItemNameInMemory() { return itemNameInMemory; }

    public static int getFinalQuantity() { return finalQuantity; }
}
