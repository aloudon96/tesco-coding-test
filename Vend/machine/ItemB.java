package machine;

public class ItemB extends Item {

    private String itemSelector = "B";
    private double itemPrice = 1.00;
    private int quantity;

    private static int finalQuantity;
    private String objectItemName = "Galaxy Caramel";

    private static String itemNameInMemory;

    public ItemB (int quantity) {
        this.quantity = quantity;
        ItemB.itemNameInMemory = objectItemName;
        ItemB.finalQuantity = quantity;
    }

    @Override
    public double getPrice() {
        return this.itemPrice;
    }

    public String getObjectItemName() { return this.objectItemName; }

    public int getQuantity() {
        return quantity;
    }

    public String getItemSelector() {
        return this.itemSelector;
    }

    public void removeOne() {
        this.quantity--;
        ItemB.finalQuantity = this.quantity;
    }

    public static String getItemNameInMemory() { return itemNameInMemory; }

    public static int getFinalQuantity() { return finalQuantity; }

}
